__doc__ = """
vau.py is a chatgpt-4.0 derived skeleton

"""
import re
import unittest

def tokenize(s):
    tokens = []
    stack = []

    def flush_stack():
        if stack:
            tokens.append("".join(stack))
            stack.clear()

    i = 0
    while i < len(s):
        char = s[i]

        if char in " \t\n":
            flush_stack()
        elif char == '(' or char == ')':
            flush_stack()
            tokens.append(char)
        elif char == '"':
            flush_stack()
            stack.append(char)
            i += 1
            while i < len(s) and s[i] != '"':
                if s[i] == '\\' and (i + 1) < len(s):
                    stack.append(s[i])
                    i += 1
                stack.append(s[i])
                i += 1
            stack.append('"')
            flush_stack()
        else:
            stack.append(char)
        i += 1

    flush_stack()
    return tokens

class TestTokenize(unittest.TestCase):
    def test_empty_input(self):
        self.assertEqual(tokenize(""), [])

    def test_single_symbol(self):
        self.assertEqual(tokenize("symbol"), ["symbol"])

    def test_multiple_symbols(self):
        self.assertEqual(tokenize("symbol1 symbol2"), ["symbol1", "symbol2"])

    def test_parentheses(self):
        self.assertEqual(tokenize("(symbol1 symbol2)"), ["(", "symbol1", "symbol2", ")"])

    def test_nested_parentheses(self):
        self.assertEqual(tokenize("(symbol3 (symbol4))"), ["(", "symbol3", "(", "symbol4", ")", ")"])

    def test_whitespace(self):
        self.assertEqual(tokenize(" symbol1   symbol2\t(symbol3\nsymbol4) "),
                         ["symbol1", "symbol2", "(", "symbol3", "symbol4", ")"])

    def test_numbers(self):
        self.assertEqual(tokenize("42 3.14"), ["42", "3.14"])

    def test_strings(self):
        self.assertEqual(tokenize('"Hello, world!"'), ['"Hello, world!"'])

    def test_mixed_input(self):
        self.assertEqual(tokenize('(define add (lambda (x y) (+ x y)))'),
                         ["(", "define", "add", "(", "lambda", "(", "x", "y", ")", "(", "+", "x", "y", ")", ")", ")"])

def parse(tokens):
    if not tokens:
        return []
    stack = []
    result = []
    for token in tokens:
        if token == "(":
            new_list = []
            if stack:
                stack[-1].append(new_list)
            stack.append(new_list)
        elif token == ")":
            if len(stack) == 1:
                result = stack.pop()
            else:
                stack.pop()
        else:
            try:
                value = int(token)
            except ValueError:
                try:
                    value = float(token)
                except ValueError:
                    if token.lower() in ['true', 't']:
                        value = True
                    elif token.lower() in ['false']:
                        value = False
                    else:
                        if token[0] == '"' and token[-1] == '"':
                            value = token[1:-1]
                        else:
                            value = token
            if stack:
                stack[-1].append(value)
            else:
                result.append(value)

    if not isinstance(result[0], list) and len(result) == 1:
        result = result[0]
    return result

class TestParse(unittest.TestCase):
    def test_empty_input(self):
        self.assertEqual(parse([]), [])

    def test_single_symbol(self):
        self.assertEqual(parse(["symbol"]), "symbol")

    def test_single_number(self):
        self.assertEqual(parse(["42"]), 42)

    def test_single_float(self):
        self.assertEqual(parse(["3.14"]), 3.14)

    def test_simple_list(self):
        self.assertEqual(parse(["(", "symbol1", "symbol2", ")"]), ["symbol1", "symbol2"])

    def test_nested_list(self):
        self.assertEqual(parse(["(", "symbol3", "(", "symbol4", ")", ")"]), ["symbol3", ["symbol4"]])

    def test_deeply_nested_list(self):
        self.assertEqual(parse(["(", "(", "(", "symbol1", "symbol2", ")", ")", ")"]), [[["symbol1", "symbol2"]]])

    def test_multiple_nested_lists(self):
        self.assertEqual(parse(["(", "symbol1", "(", "symbol2", ")", "(", "symbol3", ")", ")"]),
                         ["symbol1", ["symbol2"], ["symbol3"]])

    def test_strings(self):
        self.assertEqual(parse(['"Hello, world!"']), 'Hello, world!')

    def test_mixed_input(self):
        self.assertEqual(parse(
            ["(", "define", "add", "(", "lambda", "(", "x", "y", ")", "(", "+", "x", "y", ")", ")", ")"]),
                        ["define", "add", ["lambda", ["x", "y"], ["+", "x", "y"]]])

def sexp_to_python_list(s):
    return parse(tokenize(s))

class Env:
    def __init__(self, parent=None):
        self.parent = parent
        self.bindings = {}

    def lookup(self, symbol):
        if symbol in self.bindings:
            return self.bindings[symbol]
        elif self.parent:
            return self.parent.lookup(symbol)
        else:
            raise NameError(f"Unbound variable: {symbol}")

    def extend(self, symbols, values):
        new_env = Env(self)
        for symbol, value in zip(symbols, values):
            new_env.bindings[symbol] = value
        return new_env
def eval_vau(expr, env):
    if isinstance(expr, str):
        return env.lookup(expr)
    elif isinstance(expr, (int, float, bool)):
        return expr
    elif isinstance(expr, list) and len(expr) > 0:
        operation = expr[0]
        if operation == 'quote':
            return expr[1]
        elif operation == 'vau':
            _, params, env_param, body = expr
            return ('vau_closure', params, env_param, body, env)
        elif operation == 'if':
            _, test_expr, then_expr, else_expr = expr
            test_result = eval_vau(test_expr, env)
            if test_result:
                return eval_vau(then_expr, env)
            else:
                return eval_vau(else_expr, env)
        elif operation == 'defun':
            _, name, params, env_param, body = expr
            closure = ('vau_closure', params, env_param, body, env)
            env.bindings[name] = closure
            return name
        elif operation == 'progn':
            result = None
            for subexpr in expr[1:]:
                result = eval_vau(subexpr, env)
            return result
        else:
            func_expr, *arg_exprs = expr
            func = eval_vau(func_expr, env)
            if func[0] == 'vau_closure':
                _, params, env_param, body, func_env = func
                arg_values = [eval_vau(arg_expr, env) for arg_expr in arg_exprs]
                new_env = func_env.extend(params, arg_values)
                new_env.bindings[env_param] = env
                return eval_vau(body, new_env)
            elif func[0] == 'builtin':
                builtin_func = func[1]
                arg_values = [eval_vau(arg_expr, env) for arg_expr in arg_exprs]
                return builtin_func(*arg_values)
            elif func[0] == 'let':
                bindings, body = arg_exprs[0], arg_exprs[1]
                new_env = env.extend([(binding[0], eval_vau(binding[1], env)) for binding in bindings])
                return eval_vau(body, new_env)
            else:
                raise TypeError("Cannot apply a non-vau_closure or non-builtin function")
    else:
        raise TypeError("Invalid expression type")


def run_vau(expr):
    print(expr)
    global_env = Env()
    return eval_vau(expr, global_env)


def intake(input_str, env):
    tokens = tokenize(input_str)
    parsed_expr = parse(tokens)
    result = eval_vau(parsed_expr, env)
    return result

def classic_builtins(global_env):

    global_env.bindings.update({
            "+": ('builtin', lambda x, y: x + y),
            "-": ('builtin', lambda x, y: x - y),
            "*": ('builtin', lambda x, y: x * y),
            "/": ('builtin', lambda x, y: x / y),
    'let': ('let', None),
    })
    
def main():
    input_expr = [
        "progn",
        ["defun", "add", ["x", "y"], "env", ["+", "x", "y"]],
        ["add", 3, 4],
    ]

    initial_env = Env(None)
    
    classic_builtins(initial_env)

    result = eval_vau(input_expr, initial_env)
    print(result)  # Output: 7

    sexpr = """
    (vau (x) e
        (if x
            (quote x_is_truthy)
            (quote x_is_falsy)
        )
    )
    """
    expr = sexp_to_python_list(sexpr)
    print(expr)
    result_truthy = run_vau([expr, True])
    result_falsy = run_vau([expr, False])
    print(result_truthy)  # Output: 'x_is_truthy'
    print(result_falsy)   # Output: 'x_is_falsy'

def s_expr_repr(expr):
    if isinstance(expr, list):
        return '(' + ' '.join(s_expr_repr(e) for e in expr) + ')'
    else:
        return repr(expr)



class TestIntakeVau(unittest.TestCase):
    def setUp(self):
        self.global_env = Env(None)
        self.global_env.bindings.update({
            "+": ('builtin', lambda x, y: x + y),
            "-": ('builtin', lambda x, y: x - y),
            "*": ('builtin', lambda x, y: x * y),
            "/": ('builtin', lambda x, y: x / y),
        })

    def test_addition(self):
        result = intake("(+ 3 4)", self.global_env)
        self.assertEqual(result, 7)

    def test_subtraction(self):
        result = intake("(- 10 4)", self.global_env)
        self.assertEqual(result, 6)

    def test_multiplication(self):
        result = intake("(* 3 4)", self.global_env)
        self.assertEqual(result, 12)

    def test_division(self):
        result = intake("(/ 12 3)", self.global_env)
        self.assertEqual(result, 4)

    def test_vau_closure(self):
        intake("(defun square (x) env (* x x))", self.global_env)
        result = intake("(square 5)", self.global_env)
        self.assertEqual(result, 25)

    def test_if_true(self):
        result = intake("(if True 1 0)", self.global_env)
        self.assertEqual(result, 1)

    def test_if_false(self):
        result = intake("(if False 1 0)", self.global_env)
        self.assertEqual(result, 0)

    def test_progn(self):
        intake_expr = """(progn
                          (defun add (x y) env (+ x y))
                          (defun multiply (x y) env (* x y))
                        )"""
        intake(intake_expr, self.global_env)
        add_result = intake("(add 3 4)", self.global_env)
        multiply_result = intake("(multiply 3 4)", self.global_env)
        self.assertEqual(add_result, 7)
        self.assertEqual(multiply_result, 12)

    def test_vau_basic(self):
        intake("(defun double (x) env (* x 2))", self.global_env)
        result = intake("(double 4)", self.global_env)
        self.assertEqual(result, 8)

    def test_vau_with_environment(self):
        intake_expr = """(progn
                          (defun make-adder (x) env (vau (y) dyn_env (+ x y)))
                          (defun add5 (y) env ((make-adder 5) y))
                        )"""
        intake(intake_expr, self.global_env)
        result = intake("(add5 10)", self.global_env)
        self.assertEqual(result, 15)

    def test_vau_nested(self):
        intake_expr = """(progn
                          (defun make-multiplier (x) env (vau (y) dyn_env (* x y)))
                          (defun make-nested-multiplier (x) env (make-multiplier (make-multiplier x)))
                          (defun multiply4 (y) env ((make-nested-multiplier 2) y))
                          (multiply4 3)
                        )"""
        result = intake(intake_expr, self.global_env)
        self.assertEqual(result, 12)
        
def repl():
    global_env = Env(None)


    print("Vau Calculus REPL")
    print("Type 'exit' to exit")

    while True:
        try:
            input_str = input('V ')

            if input_str.strip() == 'exit':
                break

            result = intake(input_str, global_env)

            print(s_expr_repr(result))

        except Exception as e:
            print(f"Error: {e}")

if __name__ == "__main__":
    main()
